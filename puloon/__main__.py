from puloon import __app_name__
from .cli import app

app(prog_name=__app_name__)
